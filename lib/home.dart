import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter_firebase_auth/navigat/icecream_about.dart';
import 'package:flutter_firebase_auth/navigat/icecream_chart.dart';
import 'package:flutter_firebase_auth/navigat/icecream_home.dart';
import 'package:flutter_firebase_auth/navigat/icecream_menu.dart';
import 'package:flutter_firebase_auth/navigat/icecream_profile.dart';


class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  FirebaseAuth _auth = FirebaseAuth.instance;

  get user => _auth.currentUser;

  int _selectedIndex = 2;

  Widget getWidgets(int index){
    switch(index){
      case 0:
        return IceCreamProfile(user: user);
      case 1:
        return const IceCreamMenu();
      case 2:
        return const IceCreamHome();
      case 3:
        return const IceCreamChart();
      case 4:
        return const IceCreamAbout();
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CurvedNavigationBar(
        color: const Color.fromARGB(255, 239, 192, 145),
        height: 62,
        buttonBackgroundColor: Colors.brown,
        backgroundColor: Colors.transparent,
        index: _selectedIndex,
        onTap: (int index){
          setState((){
            _selectedIndex = index;
          });
        },
        items: const [
          Icon(Icons.person, color: Colors.white,),
          Icon(Icons.icecream_rounded, color: Colors.white,),
          Icon(Icons.home, color: Colors.white,),
          Icon(Icons.bar_chart, color: Colors.white,),
          Icon(Icons.people_alt, color: Colors.white,),
        ],
      ),
      body: getWidgets(_selectedIndex),
    );
  }
}
