import 'package:flutter/material.dart';
import 'package:flutter_firebase_auth/login.dart';
import 'package:splashscreen/splashscreen.dart';


class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
        seconds: 5,
        navigateAfterSeconds: Login(),
        image: Image.asset('assets/images/Login.png'),
        photoSize: 200.0,
        loaderColor: Colors.brown);
  }
}