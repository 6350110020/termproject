import 'package:flutter/material.dart';
import 'package:flutter_firebase_auth/ice_cream.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class IceCreamHome extends StatefulWidget {
  const IceCreamHome({Key? key}) : super(key: key);

  @override
  State<IceCreamHome> createState() => _IceCreamHomeState();
}

class _IceCreamHomeState extends State<IceCreamHome> {
  Color lightColor = const Color.fromARGB(255, 239, 192, 145);
  Color darkColor = Colors.brown;

  List<Map<String, dynamic>> types = [
    {
      "icon": Icons.icecream,
      "label": "Size S",
    },
    {
      "icon": Icons.icecream_rounded,
      "label": "Size M",
    },
    {
      "icon": Icons.icecream_sharp,
      "label": "Size L",
    },
    {
      "icon": Icons.breakfast_dining,
      "label": "bread",
    },
  ];

  List<IceCream> iceCreams = IceCreamList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Container(
                height: MediaQuery.of(context).size.height / 2 - 70,
                //color: Colors.yellow,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      iceCreamAppBar(),
                      SizedBox(height: 10,),
                      Padding(
                        padding: const EdgeInsets.only(left: 20,top: 5),
                        child: const Text(
                          "Ice-Cream Size",
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.w600,
                            color: Colors.brown,
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      iceCreamListView(),
                      SizedBox(height: 20,),
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: const Text(
                          "Popular",
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.w600,
                            color: Colors.brown,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            popularIceCream(),
          ],
        ),
      ),
    );
  }

  Widget popularIceCream() => Container(
    height: 300,
    //color: Colors.yellow,
    margin: const EdgeInsets.only(left: 20),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: iceCreams.length,
      itemBuilder: (context, index){
        final iceCream = iceCreams[index];
        return Container(
          height: 280,
          width: 230,
          margin: const EdgeInsets.only(left: 20),
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: iceCream.lightColor,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 190,
                decoration: BoxDecoration(
                  //color: Colors.yellow,
                  image: DecorationImage(
                    image: AssetImage(
                      "assets/images/${iceCream.image}",
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(height: 5,),
              Text(
                iceCream.flavor,
                style: TextStyle(
                  color: iceCream.darkColor,
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RatingBar.builder(
                    itemCount: 5,
                    itemSize: 18,
                    initialRating: iceCream.rating,
                    allowHalfRating: true,
                    itemBuilder: (context, _){
                      return Icon(
                        Icons.star_rounded,
                        color: iceCream.darkColor,
                      );
                    },
                    onRatingUpdate: (rating){},
                  ),
                  Text(
                    "${iceCream.price}",
                    style: TextStyle(
                      color: iceCream.darkColor,
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    ),
  );

  Widget iceCreamListView() => Container(
    height: 90,
    // color: Colors.yellow,
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: types.length,
      itemBuilder: (context, index) {
        final type = types[index];
        return Container(
          width: 60,
          // color: Colors.yellow,
          margin: const EdgeInsets.only(right: 30,left: 5),
          child: Column(
            children: [
              Container(
                height: 55,
                width: 55,
                decoration: BoxDecoration(
                  color: lightColor,
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Icon(
                  type["icon"],
                  size: 28,
                  color: darkColor,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                type["label"],
                style: TextStyle(
                  color: darkColor,
                ),
              ),
            ],
          ),
        );
      },
    ),
  );

  Widget iceCreamAppBar() => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: const EdgeInsets.only(top: 30),
        child: const Icon(Icons.favorite,color: Colors.pink,),
      ),
      SizedBox(width: 10,),
      Padding(
        padding: const EdgeInsets.only(top: 30),
        child: const Text(
          "Home Made \nIce-Cream",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.brown,
            fontSize: 28,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      SizedBox(width: 10,),
      Padding(
        padding: const EdgeInsets.only(top: 30),
        child: const Icon(Icons.favorite,color: Colors.pink,),
      ),
    ],
  );
}
