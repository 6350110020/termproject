import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_firebase_auth/authentication.dart';
import 'package:flutter_firebase_auth/login.dart';

class IceCreamProfile extends StatefulWidget {
  FirebaseAuth _auth = FirebaseAuth.instance;

  get user => _auth.currentUser;

  IceCreamProfile({Key? key, required User user})
      : _user = user,
        super(key: key);

  final User _user;

  @override
  _IceCreamProfileState createState() => _IceCreamProfileState();
}

class _IceCreamProfileState extends State<IceCreamProfile> {
  late User _user;
  Color lightColor = const Color.fromARGB(255, 239, 192, 145);
  Color darkColor = Colors.brown;

  Route _routeToSignInScreen() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => Login(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(-1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  @override
  void initState() {
    _user = widget._user;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(),
            ClipPath(
              clipper: MyCustomClipper(),
              child: Container(
                height: 300.0,
                color: darkColor,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 200,
                  width: 200,
                  child: CircleAvatar(
                    backgroundImage: AssetImage("assets/images/Chocolate.jpg"),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Welcome Back!',
                  style: TextStyle(
                    color: darkColor,
                    fontSize: 30,
                    fontWeight: FontWeight.bold
                  ),
                ),
                SizedBox(height: 8.0),
                Text(
                  '( ${_user.email!} )',
                  style: TextStyle(
                    color: Colors.pink,
                    fontSize: 18,
                    letterSpacing: 0.5,
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 60, vertical: 10),
                  child: FlatButton(
                    padding: EdgeInsets.all(20),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    color: lightColor,
                    onPressed: () {
                      AuthenticationHelper()
                          .signOut()
                          .then((_) => Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(builder: (contex) => Login()),
                              ));
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.logout,
                          size: 25,
                          color: darkColor,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Logout",
                          style: TextStyle(fontSize: 18, color: darkColor),
                        ),
                        SizedBox(
                          width: 110,
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 25,
                          color: darkColor,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height - 150);
    path.lineTo(0, size.height);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
